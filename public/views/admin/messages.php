<div id="messages" class="main">

	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li class="active"><a href="#0" role="tab" data-toggle="tab">Inbox</a></li>
		<li><a href="#1" role="tab" data-toggle="tab">Sent</a></li>
		<li><a href="#2" role="tab" data-toggle="tab">Trash</a></li>
		<a id="btn-compose-message" href="#" class="btn btn-default">Compose</a>
	</ul>


	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane fade in active" id="0">			
			<table class="table table-striped">
				<thead>
					<tr>
						<th>From</th>
						<th>Messages</th>
						<th>Date/Time</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>John</td>
						<td>Lorem Ipsum....</td>
						<td>n/a</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				</tfoot>
			</table>
			<a href="#" class="btn btn-default pull-left"><i class="fa fa-angle-left"></i> Prev</a>
			<a href="#" class="btn btn-default pull-right">Next <i class="fa fa-angle-right"></i></a>
		</div>
		<div class="tab-pane fade" id="1">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>To</th>
						<th>Messages</th>
						<th>Date/Time</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Mary</td>
						<td>Lorem Ipsum....</td>
						<td>n/a</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				</tfoot>
			</table>
			<a href="#" class="btn btn-default pull-left"><i class="fa fa-angle-left"></i> Prev</a>
			<a href="#" class="btn btn-default pull-right">Next <i class="fa fa-angle-right"></i></a>
		</div>
		<div class="tab-pane fade" id="2">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>From</th>
						<th>To</th>
						<th>Messages</th>
						<th>Date/Time</th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>You</td>
						<td>Mary</td>
						<td>Lorem Ipsum....</td>
						<td>n/a</td>
						<td><a href="#" title="Trash"><i class="fa fa-trash"></i></a></td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				</tfoot>
			</table>
			<a href="#" class="btn btn-default pull-left"><i class="fa fa-angle-left"></i> Prev</a>
			<a href="#" class="btn btn-default pull-right">Next <i class="fa fa-angle-right"></i></a>
		</div>
		<div class="tab-pane fade" id="3">...</div>
	</div>

</div>