<?php
include("views/partials/admin_header.php");

include("views/partials/admin_sidebar.php");
include("views/admin/profile.php");	

function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Profile.listener($);
		}); })(window, jQuery);
		var Profile = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
			}
		}		
	</script>
	<?php
}
Func::footer_hook('script');

include("views/partials/admin_footer.php");