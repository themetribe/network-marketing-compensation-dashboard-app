<?php

if(isset($_POST['a'])){
	include("models/cls_user.php");
	include("models/cls_geneology.php");
	$data = $_POST;
	$error = "";
	$user = new User();
	$geneology = new Geneology();
	$user_id = $user->add($data);
	if($user_id!=""){
		$status = $geneology->add($data, $user_id);
		if($status>0){
			$_SESSION['logged_user']=array('ID'=>$user_id);
			echo 1;
		}
	}
	exit();
}

if(!isset($_GET['afid'])){
	$sponsor_id = "00000001";
	$sponsor_name = "Giger, Kurt Roy Ruizo";
}
else{
	$sponsor_id = $_GET['afid'];
	$sponsor_name = "under cons";
}





include("views/partials/html_head.php");



?>
<div id="join-wrapper"> 
	<form role="form" action="" method="POST">
	<?php
	include("views/join/step1.php");	
	include("views/join/step2.php");
	include("views/join/step3.php");	
	?>
	</form>
</div>

















<?php
function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Join.listener($);
			Join.step1();
			Join.step2();
			Join.step3();
		}); })(window, jQuery);

		var Join = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				$(".join-containers:not(#join-step-1) .btn-primary").on('click',function(){
					if(!$(this).hasClass('btn-submit')){
						next_step = $(this).parents('.join-containers').next('.join-containers');
						next_step.addClass('active');
						$("#join-wrapper").css({
							height:'auto'
						});
						$('html,body').animate({
							 scrollTop: $(next_step).offset().top - 150
			     		}, 500);
						return false;			
					}					
				})
				$(".join-containers .btn-back").on('click',function(){
					that.back_button(this);				
					return false;
				})
			},
			step1 : function(){
				$("#join-step-1 .btn-primary").on('click',function(){
					if(!$(this).hasClass('btn-submit')){
						that.primary_button(this);	
						return false;			
					}	
				})
			},
			step2 : function(){
				$("#join-step-2 .buttons a").on('click',function(){
					$(".buttons a").removeClass('active');
					$(this).addClass('active');	
					$(".monitor iframe").attr('src','//www.youtube.com/embed/'+$(this).data('ytid')+'?rel=0');

					$("#txt_plan ."+$(this).data('type')).attr('selected',true);
				})
			},
			step3 : function() {
				
				$("form").on('submit',function(event){
					event.preventDefault();
					var valid = true;
					var error = "";
					var element = "";
					
					// check password confirm
					var pass = $("#password").val();
					var pass_c = $("#conf-password").val();
					if( pass != pass_c ){
						error = "Password not match!";
						element = "#password, #conf-password";
						valid=false;
					}
					if(!valid){
						$(element).parent().addClass('has-error')
							.find('label').append("<small>Error : "+error);
						$('html,body').animate({
							 scrollTop: $(element).offset().top - 100
			     		}, 500);
					}
					else{
						that.createUser(this);
					}
					return false;
				});
			},
			createUser : function(_this){
				show_loader($);
				var _data = "a=1&"+$(_this).serialize();;
				$.post(window.location.href,_data, function(data){
					if($.trim(data)==1){
						window.location.href="/myaccount/new_user";
					}
					else if($.trim(data)==2){
						alert('Something is wrong with the transaction. Please contact the site administrator.')
					}
					close_loader($);
					console.log(data);
				});
			},
			primary_button : function(_this){
				$(".join-containers").removeClass('inactive');
				$(".join-containers").removeClass('active');
				$(_this).parents('.join-containers').addClass('inactive');
				setTimeout(function(){
					$('html,body').animate({
						 scrollTop: $("body").offset().top
		     		}, 500);
					next_step = $(_this).parents('.join-containers').next('.join-containers');
					next_step.addClass('active');
					height = next_step.outerHeight()+100;
					$("#join-wrapper").css('height',height);
					
				}, 900);	
			}
		}
	</script>
	<?php
}
Func::footer_hook('script');
include("views/partials/html_foot.php");
?>