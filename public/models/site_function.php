<?php
class Func{
	public static function body_class(){
		$string = 'class="';
		// DETECT PAGE
		if(isset($_GET['page']))
			$string.=$_GET['page']." ";

		// DETECT BROWSER
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 10')!==false) $browser = "ie10";
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9')!==false) $browser = "ie9";
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8')!==false) $browser = "ie8";
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7')!==false) $browser = "ie7";	
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox')!==false) $browser = "firefox";
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'Safari')!==false) $browser = "safari";
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')!==false) $browser = "chrome";

		$string.=$browser." ";		
		$string = substr($string, 0,-1);
		$string .='"';
		echo $string;
	}
	public static function init(){
		global $function_name;
		$function_name = '';	
	} 
	public static function footer_hook($_func_name){
		global $function_name;
		$function_name = $_func_name;
	}
	public static function execute_footer_hook(){
		global $function_name;
		if(function_exists($function_name))
			$function_name();
	}
	public static function loader_modal(){ ?>
		<div class="modal fade" id="loader_modal"> 
			<div class="modal-dialog">
				<img src="<?php echo SITE_URL ?>/assets/img/loader.gif" />
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<?php
	}	
}

?>