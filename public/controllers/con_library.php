<?php
include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");
include("views/admin/library.php");

function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Library.listener($);
		}); }) (window, jQuery);

		var Library = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				$("#menu > ul > li > a").on('click',function(){
					that.menudropdown(this);
				});
				$(".contents .item").on('click',function(){
					Modal.hasHeader = 
					Modal.hasButton = false;
					Modal.addId = "product_detail_modal";
					Modal.contents =
						'<div class="product-image"> '+
						'	<img src="http://inlight.dev/assets/img/book-sample.jpg" />'+
						'</div>'+
						'<div class="product-description">'+
						'	<h3>The Fault In Our Stars</h3>'+
						'	<p>Lorem ipsum...</p>'+
						'	<a href="#" class="btn btn-primary btn-lg">Read</a>'+
						'</div>'+
						'<a href="#" class="btn btn-default"><i class="fa fa-star"></i> Add To Favorite</a>';
					Modal.show($);
				})

			},
			menudropdown : function(that){
				$(that).parent().find('ul').toggleClass('active');
			},

		}
	</script>
	<?php	
}
Func::footer_hook('script');
include("views/partials/admin_footer.php");