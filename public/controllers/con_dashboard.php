<?php
include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");

function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Dashboard.listener($);
		}); })(window, jQuery);
		var Dashboard = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
			}
		}		
	</script>
	<?php
}
Func::footer_hook('script');

include("views/admin/dashboard.php");	

include("views/partials/admin_footer.php");