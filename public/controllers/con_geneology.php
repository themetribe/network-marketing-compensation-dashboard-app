<?php
include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");
include("views/admin/geneology.php");

function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Geneology.listener($);
		}); })(window, jQuery);
		var Geneology = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				$("#btn-textual").on('click',function(){
					that.show_textual(this);
					return false;
				})
				$("#btn-graphical").on('click',function(){
					that.show_graphical(this);
					return false;
				})
				$("#btn-search").on('click',function(){
					that.show_search(this);
					return false;
				})
				$("#btn-summary").on('click',function(){
					that.show_summary(this);
					return false;
				})
				that.setpopupcontrol();
			},
			show_textual : function(that){
				$("#graphical").removeClass('active');
				$("#textual").addClass('active');
			},
			show_graphical : function(that){
				$("#graphical").addClass('active');
				$("#textual").removeClass('active');
			},
			show_search : function(that){
				Modal.hasHeader = 
				Modal.hasButton = false;
				Modal.addId = "search_geneology_modal";
				Modal.contents = 
					'<form role="form">' +
						'<div class="form-group">'+
							'<label for="">Date From:</label>'+
							'<input type="text" class="form-control" placeholder="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Date To:</label>'+
							'<input type="text" class="form-control" placeholder="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">ID Number:</label>'+
							'<input type="text" class="form-control" placeholder="" value="04272782" disabled />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Account Name:</label>'+
							'<input type="text" class="form-control" placeholder="" value="GIGER, KURT ROY RUIZO" disabled />'+
						'</div>'+
						'<a class="btn btn-primary" href="#">Search</a>'+
					'</form>'
				Modal.show($);
			},
			show_summary : function(that){
				Modal.hasHeader = 
				Modal.hasButton = false;
				Modal.addId = "summary_geneology_modal";
				Modal.contents = 
					'<table class="table table-striped" id="textual-data">' +
					'	<thead>' +
					'		<tr>'+
					'			<th>&nbsp;</th>'+
					'			<th>Paid</th>'+
					'			<th>FS</th>'+
					'			<th>Total Heads</th>'+
					'		</tr>'+
					'	</thead>'+
					'	<tbody>'+
					'		<tr>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'		</tr>'+
					'		<tr>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'			<td>0</td>'+
					'		</tr>'+
					'	</tbody>'+
					'</table>';
				Modal.show($);
			},
			setpopupcontrol : function(){
				console.log('test');
				$("#graphical-data .user").each(function(){
					console.log(this);
					var data = 	'<div class="popup-control">'+
								'<a href="/messages" class="btn-default btn">Message</a>'+
								'<a href="/profile" class="btn-default btn">View Profile</a>'+
								'</div>';
					$(this).prepend(data);
				})
			}
		}		
	</script>
	<?php	
}
Func::footer_hook('script');
include("views/partials/admin_footer.php");