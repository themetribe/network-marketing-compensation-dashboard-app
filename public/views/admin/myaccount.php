<div id="myaccount" class="main">

<div class="block" id="main-buttons">
	<a href="#" id="btn-personal" class="btn-toggle active center"><i class="fa fa-columns"></i> Personal Info</a>
	<a href="#" id="btn-security" class="btn-toggle center"><i class="fa fa-th-list"></i> Security</a>
</div>

<div class="wrapper">
	
	<div class="block row" id="personal-info-cont">
		<div class="col-md-9">
			<form role="form">
				<div class="form-group">
					<label for="">Complete Address:</label>
					<textarea class="form-control" placeholder="Please enter complete address"></textarea>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label for="">City:</label>
						<input type="text" class="form-control" placeholder="Please enter city" />
					</div>
					<div class="form-group col-md-6">
						<label for="">Province:</label>
						<input type="text" class="form-control" placeholder="Please enter province" />
					</div>	
				</div>
				
				<div class="row">
					<div class="form-group col-md-4">
						<label for="">Birth Date:</label>
						<input type="text" class="form-control" placeholder="DD/MM/YY" />
					</div>
					<div class="form-group col-md-4">
						<label for="">Gender:</label>
						<input type="text" class="form-control" placeholder="Please enter gender" />
					</div>
					<div class="form-group col-md-4">
						<label for="">Status:</label>
						<input type="text" class="form-control" placeholder="Please enter satus" />
					</div>	
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						<label for="">Email:</label>
						<input type="text" class="form-control" placeholder="Please enter email" />
					</div>
					<div class="form-group col-md-6">
						<label for="">Phone:</label>
						<input type="text" class="form-control" placeholder="xxx-xxxx-xxxx" />
					</div>
				</div>
				<hr />

				<div class="form-group">
					<label for="">Payout Type:</label>
					<select class="form-control">
						<option value="" disabled>[Choose Payout Type]</option>
						<option>Bank</option>
						<option>Luwelyer</option>
					</select>			
				</div>
				<div class="form-group">
					<label for="">Account Name:</label>
					<input type="text" class="form-control" placeholder="Please enter account name" />
				</div>
				<div class="form-group">
					<label for="">Account Number:</label>
					<input type="text" class="form-control" placeholder="Please enter account number" />
				</div>
				<input type="submit" class="btn btn-default" value="Save" />
			</form>
		</div>
		<div class="col-md-3">
			<div class="block" id="my-sponsor">
				<h3>My Sponsor</h3>
				<img src="<?php echo SITE_URL ?>/assets/img/upline.jpg" />
				<div class="sponsor-info">
					<strong>Ariza Arcan</strong>
					<span class="num">0923 547 7338</span>
					<span class="email">arizacarcan@gmail.com</span>
				</div>				
			</div>
		</div>
	</div>

	<div class="block" id="security-cont">
		<form role="form">
			<div class="row">
				<div class="col-md-6">
					<h3>Change Password</h3>
					<div class="form-group">
						<label for="">Old Password:</label>
						<input type="text" class="form-control" placeholder="Please enter your old password" />
					</div>
					<div class="form-group">
						<label for="">New Password:</label>
						<input type="text" class="form-control" placeholder="Please enter your new password" />
					</div>
					<div class="form-group">
						<label for="">Retype New Password:</label>
						<input type="text" class="form-control" placeholder="Please retype your new password" />
					</div>
					<input type="submit" class="btn btn-default" value="Update Password" />
				</div>
				<div class="col-md-6">
					<h3>Change PIN</h3>
					<div class="form-group">
						<label for="">What is the first question?</label>
						<input type="text" class="form-control" placeholder="" />
					</div>
					<div class="form-group">
						<label for="">Who is the first question?</label>
						<input type="text" class="form-control" placeholder="" />
					</div>
					<div class="form-group">
						<label for="">When is the first question?</label>
						<input type="text" class="form-control" placeholder="" />
					</div>
					<input type="submit" class="btn btn-default" value="Update PIN" />
				</div>
			</div>
		</form>
	</div>

</div>