<div id="geneology" class="main">

	<div id="geneology-mode" class="clearfix">
		<a href="#" id="btn-graphical" class="btn-toggle active center"><i class="fa fa-columns"></i> Graphical</a>
		<a href="#" id="btn-textual" class="btn-toggle center"><i class="fa fa-th-list"></i> Textual</a>
	</div>
	
	<div class="clearfix active" id="graphical">

		<?php
		/* WILL BE USED LATER
		<div class="pull-left" >
			<a href="#"><i class="fa fa-angle-double-up"></i> Top</a>
			<a href="#"><i class="fa fa-angle-up"></i> Up</a>
		</div> */ ?>

		<div id="legend">
			<strong>Legend</strong>
			<ul>
				<li><i class="sprite blue-collar-small"></i> Blue Tie - Business</li>
				<li><i class="sprite yellow-collar-small"></i> Yellow Tie - Pro</li>
				<li><i class="sprite grey-collar-small"></i> Grey Tie - Starter</li>
				<li><i class="sprite blue-circle-small"></i> Blue Background - Direct Referral</li>
				<li><i class="sprite orange-circle-small"></i> Orange Background - Indirect Referral</li>
				<li><i class="sprite blue-arrow"></i> Blue Lines - Healthy Leg</li>
				<li><i class="sprite red-arrow"></i> Red Lines - Unhealthy Leg</li>
			</ul>
		</div>

		<div id="graphical-data">
			<div class="stage stage-1">
				<div class="user referral-direct">
					<i class="sprite user-premier avatar-default"></i>
				</div>
				<div class="connector">
					<div class="left unhealthy">
						<span class="l1"></span>
						<span class="l2"></span>
						<span class="l3"></span>
					</div>
					<div class="right healthy">
						<span class="l1"></span>
						<span class="l2"></span>
						<span class="l3"></span>
					</div>
				</div>
				<div class="stage stage-2">
					<div class="left">
						<a href="#" class="user referral-indirect">
							<i class="sprite user-basic avatar-default"></i>
							
						</a>						
						<div class="connector">
							<div class="left">
								<span class="l1"></span>
								<span class="l2"></span>
								<span class="l3"></span>
							</div>
							<div class="right">
								<span class="l1"></span>
								<span class="l2"></span>
								<span class="l3"></span>
							</div>
						</div>
						<div class="stage stage-3">
							<div class="left">
								<a href="#" class="user">
									<i class="sprite avatar-default-sm"></i>
								</a>
							</div>
							<div class="right">
								<a href="#" class="user">
									<i class="sprite avatar-default-sm"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="right">
						<a href="#" class="user referral-direct">
							<i class="sprite user-premier avatar-default"></i>
						</a>
						<div class="connector">
							<div class="left healthy">
								<span class="l1"></span>
								<span class="l2"></span>
								<span class="l3"></span>
							</div>
							<div class="right healthy">
								<span class="l1"></span>
								<span class="l2"></span>
								<span class="l3"></span>
							</div>
						</div>
						<div class="stage stage-3">
							<div class="left">
								<a href="#" class="user referral-indirect">
									<i class="sprite user-basic-small avatar-default-sm"></i>
								</a>
							</div>
							<div class="right">
								<a href="#" class="user referral-direct">
									<i class="sprite user-basic-small avatar-default-sm"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>	

	<div class="clearfix" id="textual">
		<div class="buttons">
			<a href="#" id="btn-search" class="btn btn-default"><i class="fa fa-search"></i> Search</a>
			<a href="#" id="btn-summary" class="btn btn-default"><i class="fa fa-calculator"></i> Summary</a>
		</div>
		<table class="table table-striped" id="textual-data">
			<thead>
				<tr>
					<th>Level</th>
					<th>Upline</th>
					<th>ID No</th>
					<th>Name</th>
					<th>Group</th>
					<th>Side</th>
					<th>RegDate</th>
					<th>Upgrade Date</th>
					<th>Sponsor</th>
					<th>Rank</th>
				</tr>
			</thead>
			<tbody>
				<?php for($x=1;$x<=10;$x++) : ?>
				<tr>
					<td>1</td>
					<td>04272782</td>
					<td>00372187</td>
					<td>GIGER, KURT ROY RUIZO</td>
					<td>A</td>
					<td>A</td>
					<td>12/20/2012</td>
					<td>12/20/2012</td>
					<td>04272782</td>
					<td>D</td>
				</tr>
				<?php endfor; ?>
			</tbody>
		</table>

	</div>	

</div>