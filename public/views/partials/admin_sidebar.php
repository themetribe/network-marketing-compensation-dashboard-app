<div id="sidebar">
	<span class="extension"></span>
	<ul>
		<li><a href="/dashboard"><i class="fa fa-lightbulb-o"></i>Dashboard</a></li>
		<li><a href="/geneology"><i class="fa fa-line-chart"></i>Geneology</a></li>
		<li><a href="/ewallet"><i class="fa fa-cc-mastercard"></i>E-Wallet</a></li>
		<li><a href="/library"><i class="fa fa-book"></i>Library</a></li>
		<li><a href="/support"><i class="fa fa-support"></i>Support Ticket</a></li>
		<li><a href="/messages"><i class="fa fa-wechat"></i>Messages</a></li>
		<li><a href="/myaccount"><i class="fa fa-cogs"></i>My Account</a></li>
		<li><a href="/profile"><i class="fa fa-user"></i>My Profile</a></li>
	</ul>
</div>