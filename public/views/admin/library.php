<div class="main" id="library">
	<div class="block" id="menu">
		<ul>
			<li><a href="#" class="btn btn-default"><i class="fa fa-heart"></i> Favorites</a></li>
			<li>
				<a href="#" class="btn btn-default"><i class="fa fa-futbol-o"></i> Categories</a>
				<ul>
					<li><a href="#">Body</a></li>
					<li><a href="#">Mind</a></li>
					<li><a href="#">Soul</a></li>
					<li><a href="#">Supplemental</a></li>
				</ul>
			</li>
			<li>
				<a href="#" class="btn btn-default"><i class="fa fa-caret-right"></i> Sort By</a>
				<ul>
					<li><a href="#">Alphabetical</a></li>
					<li><a href="#">Date</a></li>
					<li><a href="#">Popular</a></li>
					<li><a href="#">Ratings</a></li>
				</ul>
			</li>
			<li>
				<a href="#" class="btn btn-default"><i class="fa fa-search"></i> Search</a>
				<ul>
					<li>
						<div class="search-form">
							<form role="form">
								<div class="form-group">
									<label for="">Name</label>
									<input type="text" class="form-control" id="" placeholder="">
								</div>
								<input type="submit" value="Submit" />
							</form>
						</div>					
					</li>
				</ul>
			</li>
		</ul>
	</div>

	<div class="contents">
		<?php for($x=1;$x<=10;$x++) : ?>
			<div class="item">
				<a href="#"></a>
				<span class="favorite center"><i class="fa fa-heart"></i></span>
				<img src="<?php SITE_URL ?>/assets/img/book-sample.jpg" />				
			</div>
		<?php endfor; ?>
	</div>
</div>