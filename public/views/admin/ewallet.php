<div id="ewallet" class="main">
	<iframe width="560" height="315" src="//www.youtube.com/embed/-8D4j5CH8TA?rel=0" frameborder="0" allowfullscreen></iframe>
	<div class="block row">
		<div class="pull-left" id="transact-form">
			<div id="step1" class="tform active">
				<a href="#" id="btn-widthraw" class="btn btn-primary btn-lg"><i class="fa fa-star"></i>Widthraw Money</a>
				<a href="#" id="btn-buy" class="btn btn-info btn-lg"><i class="fa fa-ticket"></i>Buy Activation Codes</a>
				<a href="#" id="btn-upgrade" class="btn btn-success btn-lg"><i class="fa fa-smile-o"></i>Upgrade Account</a>
			</div>
			<div id="step2-a" class="tform">
				<a href="#" class="btn-back">&laquo; Back</a>
				<h3>Widthraw Money</h3>
				<form role="form">
					<div class="form-group">
						<label for="">Amount:</label>
						P<input type="text" class="form-control" placeholder="" />
					</div>
					<div class="form-group">
						<label for="">PIN:</label>
						<input type="text" class="form-control" placeholder="" />
					</div>
					<input type="submit" class="btn btn-primary" value="Save" />
				</form>
			</div>
			<div id="step2-b" class="tform">
				<a href="#" class="btn-back">&laquo; Back</a>
				<h3>Buy Activation Codes</h3>
				<form role="form">
					<div class="form-group">
						<label for="">Quantity:</label>
						<input type="text" class="form-control" placeholder="" />
					</div>
					<div class="form-group">
						<label for="">PIN:</label>
						<input type="text" class="form-control" placeholder="" />
					</div>
					<div class="form-group">
						<label for="">Account Type:</label>
						<select class="form-control">
							<option value="" disabled>[Choose Account Type]</option>
							<option>Starter</option>
							<option>Pro</option>
							<option>Business</option>
						</select>			
					</div>
					<input type="submit" class="btn btn-primary" value="Save" />
				</form>
			</div>
			<div id="step2-c" class="tform">
				<a href="#" class="btn-back">&laquo; Back</a>
				<h3>Upgrade Account</h3>
				<form role="form">
					<div class="form-group">
						<label for="">Upgrade Account To:</label>
						<select class="form-control">
							<option value="" disabled>[Choose Account Type]</option>
							<option>Pro</option>
							<option>Business</option>
						</select>			
					</div>
					<input type="submit" class="btn btn-primary" value="Save" />
				</form>
			</div>
		</div>
		<div class="pull-right" id="balance-summary">
			<div class="alert alert-warning fade in" role="alert">
				<strong>Available Balance</strong>
				<h2>P 100,000</h2>
				<strong>Total Earnings</strong>
				<h2>P 300,000</h2>
				<strong>Codes to Purchase</strong>
				<h2>0</h2>
			</div>			
		</div>
	</div>

	<div class="block clearfix">
		<div class="pull-right">
			<a href="#" class="btn btn-default"><i class="fa fa-money"></i> Widthraw</a>
			<a href="#" class="btn btn-default"><i class="fa fa-shopping-cart"></i> Purchase</a>
		</div>
		<br style="clear:both;" />
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Date</th>
					<th>Remarks</th>
					<th>Reference</th>
					<th>Gross</th>
					<th>Fee</th>
					<th>Net</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="6">You do not have corresponding data for this view yet.</td>
				</tr>
			</tbody>
		</table>
		<a class="btn btn-default pull-right" href="#">Load More</a>
	</div>
	<br />
	<div class="block">
		<div class="alert alert-warning fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<strong>Important Note</strong>
			<p>You can make a Payout Request anytime, and the fund you requested shall be credited to your payout account anytime within 10-15 working days (working days meaning holidays and weekends are NOT included in the count / sa pagbilang ng working days, HINDI kasama sa pagbilang ang Sabado at Linggo at mga national holidays). To know if you have already received your commissions, refer to the Remarks column below (under the Submit button). If you see the word PAID, it means that the specific payout which has the word PAID has already been transferred to your chosen payout processor.</p>
			<p>Before making a Payout Request, make sure that you indicated all the required information for your payment processor in the Payout Detail box in the Edit Profile section, as follows...</p>
			<ul>
				<li>For bank accounts (BDO, or UnionBank), your full name, branch, and account number are required.</li>
				<li>For BDO cash cards, your full name, card number, and cellphone number are required.</li>
				<li>For Smart Money, your 16-digit card number is required (NOT your Smart cellphone number).</li>
				<li>For Globe GCash, your Globe cellphone number is required (must be registered as a GCash phone number).</li>
				<li>For MoneyGram,	LBC, MLhuillier, and Cebuana Lhuillier, your full name, town or city, and country of residence are required.</li>
				<li>For Payza, your registered Payza email address is required.</li>
			</ul>
	    </div>
	</div>
</div>