<?php
/*
This class is intended to be used in the future upgrades.
*/
class userMeta extends DB{
	public function __construct(){
		parent::__construct(); 
		$this->table = "user_meta";
	}
}