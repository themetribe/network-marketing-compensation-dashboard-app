<?php
define("FRONT_PAGE","con_home");

if (preg_match('/.dev/', $_SERVER['HTTP_HOST'])) {
     define( 'ENV', 'local' );
} 
else {
	define( 'ENV', 'live' );
}

switch (ENV) {
    case 'live':
    	define("DBHOST","#");
		define("DBUSER","#");
		define("DBPASS","#");
		define("DBNAME","#");
		define("SITE_URL","#");
        break;
    default:
        // Database
		define("DBHOST","localhost");
		define("DBUSER","root");
		define("DBPASS","");
		define("DBNAME","inlight");
		define("SITE_URL","http://inlight.dev");
        break;
}