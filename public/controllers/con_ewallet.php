<?php
include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");
include("views/admin/ewallet.php");	


function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			EWallet.listener($);
		}); })(window, jQuery);

		var EWallet = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				$("#btn-widthraw").on('click',function(){
					that.show_widthraw(this);
				});
				$("#btn-buy").on('click',function(){
					that.show_buy(this);
				});
				$("#btn-upgrade").on('click',function(){
					that.show_upgrade(this);
				});
				$(".btn-back").on('click',function(){
					that.show_step1(this);
				});
			},
			show_widthraw : function(that){
				$(that).parent().toggleClass('active');
				$("#step2-a").toggleClass('active');
			},
			show_buy : function(that){
				$(that).parent().toggleClass('active');
				$("#step2-b").toggleClass('active');
			},
			show_upgrade : function(that){
				$(that).parent().toggleClass('active');
				$("#step2-c").toggleClass('active');
			},
			show_step1 : function(that){
				$(that).parent().toggleClass('active');
				$("#step1").toggleClass('active');
			}
		}		
	</script>
	<?php
}
Func::footer_hook('script');

include("views/partials/admin_footer.php");