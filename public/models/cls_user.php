<?php

class User extends DB{
	public function __construct(){
		parent::__construct(); 
		$this->table = "user";
	}
	public function add($data){
		unset($data['a']);
		unset($data['sponsor_name']); //we don't need this since we're storing the sposor_id already

		$data['status']=0;
		$data['registered_date'] = date("Y-m-d H:i:s");

		return $this->save($data);
	}
}