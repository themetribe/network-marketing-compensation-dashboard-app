<div id="join-step-2" class="join-containers">
	<h2>Watch the video Below and learn all the details.</h2>
	<p>When people join Inlight Ultd they either pay for the Basic Membership or the Premier Membership Product.</p>
	<div class="buttons">
		<a href="#" data-ytid="RL-lfcihHvU" data-type="basic" id="btn-basic" class="btn btn-default center"><i class="fa fa-bicycle"></i> Basic</a>
		<a href="#" data-ytid="NtnBQ5mDD1U" data-type="premier" id='btn-premier' class="active btn btn-default center"><i class="fa fa-car"></i> Premier</a>
		<a href="#" data-ytid="W8ceZlU1dHg" data-type="investor" id='btn-investor' class="btn btn-default center"><i class="fa fa-plane"></i> Investor</a>		
	</div>
	<div class="monitor">
		<span class="shadow"></span>
		<iframe src="//www.youtube.com/embed/NtnBQ5mDD1U?rel=0" frameborder="0" allowfullscreen></iframe>
	</div>
	
	<a href="#" class="btn btn-primary center">Click Here to Join Now</a>
</div>