(function(global, $){ $(document).ready(function(){
	$('.datepicker').datepicker();
}); })(window, jQuery);


function show_loader($,element_force_hide){
	$("#loader_modal").modal({
		show 	: true,
		backdrop: 'static',
  		keyboard: true
	});
	if(element_force_hide!=''){
		$(element_force_hide).addClass('tobehind');
	}
}
function close_loader($,element_force_hide){
	setTimeout(function(){
		$("#loader_modal").modal('hide');
		if(element_force_hide!=''){
			$(element_force_hide).removeClass('tobehind');
		}
	},1000)	
}