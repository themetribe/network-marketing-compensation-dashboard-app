<div class="block hero-full full-height Vcenter">
	<div class="container">
		<h1>Big Headlines Here</h1>
		<p class="sub-heading">Support your headlines with sub texts</p>
		<a href="#" class="btn btn-default" id="btn_get-started">Get Started</a>
		<div class="menu">
			<a class="btn btn-default" href="#">About Us</a>
			<a class="btn btn-default" href="#">Products</a>
			<a class="btn btn-default" href="#">Opportunity</a>
			<a class="btn btn-default" href="#">Member Login</a>
		</div>
	</div>      
</div>
