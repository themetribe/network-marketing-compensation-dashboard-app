<?php
include("views/partials/admin_header.php");
include("views/partials/admin_sidebar.php");
include("views/admin/messages.php");

function script() { ?>
	<script>
		(function(global, $){ $(document).ready(function(){
			Messages.listener($);
		}); })(window, jQuery);
		var Messages = {
			that : null, $ : null,
			listener : function($){
				that=this; $ = $;
				$("#btn-compose-message").on('click',function(){
					that.show_compose();
				})
			},
			show_compose : function(){
				Modal.hasHeader = 
				Modal.hasButton = false;
				Modal.addId = "compose_message_modal";
				Modal.contents = 
					'<form role="form">' +
						'<div class="form-group">'+
							'<label for="">To:</label>'+
							'<input type="text" class="form-control" placeholder="" />'+
						'</div>'+
						'<div class="form-group">'+
							'<label for="">Messages:</label>'+
							'<textarea class="form-control" placeholder=""></textarea>'+
						'</div>'+
						'<a class="btn btn-primary" href="#">Submit Message</a>'+
					'</form>'
				Modal.show($);
			}
		}		
	</script>
<?php
}
Func::footer_hook('script');
include("views/partials/admin_footer.php");
